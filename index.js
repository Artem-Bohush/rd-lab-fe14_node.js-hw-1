const http = require('http');
const fs = require('fs');
const url = require('url');
const path = require('path');

const writeLog = require('./utils/writeLog');
const sendHTML = require('./utils/sendHTML');
const sendJSON = require('./utils/sendJSON');
const validateParams = require('./utils/validateParams');
const handleError = require('./utils/errorHandler');

const PORT = process.env.PORT || 8080;

const server = http.createServer((req, res) => {
  try {
    const { method } = req;
    const { pathname, query } = url.parse(req.url, true);

    if (method === 'POST') {
      if (pathname === '/file') {
        const wrongParameters = validateParams(query, 'filename', 'content');
        if (wrongParameters.length) {
          handleError(new Error(`Missing parameters or missing its value: ${wrongParameters}`), res, 400);
          return;
        }
        fs.writeFile(
          path.join(__dirname, 'files', query.filename),
          query.content,
          (err) => {
            if (err) {
              handleError(err, res);
              return;
            }
            writeLog('writeFile', query.filename);
            sendHTML(query.content, res);
          },
        );
      } else {
        sendJSON({ message: 'NOT FOUND' }, res, 404);
      }
    } else if (method === 'GET') {
      if (pathname === '/logs') {
        const { from, to } = query;
        fs.readFile(
          path.join(__dirname, 'db', 'logs.json'),
          'utf-8',
          (err, data) => {
            if (err) {
              handleError(err, res);
              return;
            }
            try {
              const logsObj = JSON.parse(data);
              if (!logsObj.logs) {
                logsObj.logs = [];
              }
              logsObj.logs = logsObj.logs.filter((log) => {
                if (from && to) return log.time >= from && log.time <= to;
                if (from) return log.time >= from;
                if (to) return log.time <= to;
                return true;
              });
              sendHTML(JSON.stringify(logsObj), res);
            } catch (error) {
              handleError(error, res);
            }
          },
        );
      } else if (pathname.indexOf('/file/') === 0) {
        const fileName = pathname.slice(6);
        fs.readFile(
          path.join(__dirname, 'files', fileName),
          'utf-8',
          (err, data) => {
            if (err) {
              handleError(err, res, 400);
              return;
            }
            writeLog('readFile', fileName);
            sendHTML(data, res);
          },
        );
      } else {
        sendJSON({ message: 'NOT FOUND' }, res, 404);
      }
    } else {
      sendJSON({ message: 'METHOD NOT ALLOWED' }, res, 405);
    }
  } catch (error) {
    handleError(error, res);
  }
});

server.listen(PORT, () => {
  console.log(`Server is listening on ${PORT} port...`);
});

module.exports = () => server;
