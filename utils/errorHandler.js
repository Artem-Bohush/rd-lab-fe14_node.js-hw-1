module.exports = (error, res, status = 500) => {
  res.writeHead(status, { 'Content-Type': 'text/json' });
  res.end(JSON.stringify({ message: error.message }));
};
