module.exports = {
  writeFile: (fileName) => ({
    message: `New ﬁle with name ${fileName} saved`,
    time: new Date().getTime(),
  }),
  readFile: (fileName) => ({
    message: `New ﬁle with name ${fileName} was read`,
    time: new Date().getTime(),
  }),
};
