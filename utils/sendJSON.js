module.exports = (obj, res, status = 200) => {
  res.writeHead(status, { 'Content-Type': 'text/json' });
  res.end(JSON.stringify(obj));
};
