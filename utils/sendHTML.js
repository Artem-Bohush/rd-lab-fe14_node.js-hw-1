module.exports = (str, res, status = 200) => {
  res.writeHead(status, { 'Content-Type': 'text/html; charset=utf-8' });
  res.end(str);
};
