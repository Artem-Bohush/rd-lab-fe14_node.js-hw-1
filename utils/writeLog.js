const fs = require('fs');
const path = require('path');

const logGenerator = require('./logGenerator');

module.exports = (logType, filename) => {
  const newLog = logGenerator[logType](filename);
  fs.readFile(
    path.join(__dirname, '../db', 'logs.json'),
    'utf-8',
    (err, data) => {
      if (err) {
        console.log(err);
        return;
      }
      try {
        const logsObj = JSON.parse(data);

        if (logsObj.logs) {
          logsObj.logs.push(newLog);
        } else {
          logsObj.logs = [newLog];
        }

        fs.writeFile(
          path.join(__dirname, '../db', 'logs.json'),
          JSON.stringify(logsObj),
          (e) => {
            if (e) {
              console.log(err);
            }
          },
        );
      } catch (error) {
        console.log(error);
      }
    },
  );
};
