module.exports = (query, ...params) => {
  const wrongParameters = [];
  params.forEach((p) => {
    if (!query[p] || query[p].trim() === '') {
      wrongParameters.push(p);
    }
  });
  return wrongParameters;
};
