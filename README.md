# RD LAB FE14 NODEJS HOMEWORK №1

## Use NodeJS to implement web server.

### Requirements:
- Use standard http module to implement simple web-server;
- Use fs module to create/modify ﬁles in ﬁle system;
- Organize all logs inside one `JSON` ﬁle;
- Write every request info to array in `JSON` ﬁle.

### Acceptance criteria:
- Server saves ﬁle on `POST/ﬁle` request and responds with `200` status, use 
`ﬁlename` and `content` url query params to transfer ﬁle data;
- Server returns ﬁle content on `GET/ﬁle/:ﬁlename` request, use `ﬁlename` url 
parameter to determine what ﬁle client wants to retrieve;
- In case there are no ﬁles with such name found, return `400` status;
- Server responds to `GET/logs` request with all saved logs as `JSON`;
- Every request information can be found in `JSON` ﬁle on server side;
- `Gitlab` repo link and project id are saved in Google spreadsheets by [link](https://docs.google.com/spreadsheets/d/1n91rWSw76qjC2KIHXd5CpdegQKaBbolVo_vbByCB5hk/edit#gid=0).

### Optional criteria:
- Server handles errors and validates input params for `POST/ﬁle` request;
- Ability to get logs for speciﬁed date range (`from` and `to` url query params as timestamps);

**Notes:**

1. All information about request can be found in request object;
2. `JSON` ﬁle should look like:
```json
{
  "logs": [
    {
      "message": "New ﬁle with name book.txt saved",
      "time": 97346772843
    }
  ]
}
```
Use same structure for `GET/logs` response.
